﻿//Realizar una expresión regular que detecte emails correctos.
<?php
function valida_email($str){
  $matches = null;
  return (1 === preg_match('/^[A-z0-9\\._-]+@[A-z0-9][A-z0-9-]*(\\.[A-z0-9_-]+)*\\.([A-z]{2,6})$/', $str, $matches));
}

//Realizar una expresión regular que detecte Curps Correctos
/^([A-Z][AEIOUX][A-Z]{2}\d{2}(?:0[1-9]|1[0-2])(?:0[1-9]|[12]\d|3[01])[HM](?:AS|B[CS]|C[CLMSH]|D[FG]|G[TR]|HG|JC|M[CNS]|N[ETL]|OC|PL|Q[TR]|S[PLR]|T[CSL]|VZ|YN|ZS)[B-DF-HJ-NP-TV-Z]{3}[A-Z\d])(\d)$/


//Realizar una expresión regular que detecte palabras de longitud mayor a 50 formadas solo por letras.
^[a-zA-Z]*{0,50}$

//Crea una función para escapar los símbolos especiales.
preg_quote($keywords, '/')

//Crear una expresion regular para detectar números decimales.
^[0-9]*(\.[0-9]+)?$
