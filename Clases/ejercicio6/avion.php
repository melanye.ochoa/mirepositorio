<?php  
include_once('transporte.php');

class avion extends transporte{

		private $numero_turbinas;
		
		//sobreescritura de constructor
		public function __construct($nom,$vel,$com,$tur){
			parent::__construct($nom,$vel,$com);
			$this->numero_turbinas=$tur;
		}

		// sobreescritura de metodo
		public function resumenAvion(){
			$mensaje=parent::crear_ficha();
			$mensaje.='<tr>
						<td>Numero de turbinas:</td>
						<td>'. $this->numero_turbinas.'</td>				
					</tr>';
			return $mensaje;
		}
	}

$mensaje='';


if (!empty($_POST)){
	//declaracion de un operador switch
	switch ($_POST['tipo_transporte']) {
		case 'terrestre':
			$carro1= new carro('carro','200','gasolina','4');
			$mensaje=$carro1->resumenCarro();
			break;	
	}

}

?>