/*
La funci�n "substr" devuelve parte de una cadena.
Ejemplo:
*/
<?php
echo substr('abcdef', 1);     // bcdef
echo substr('abcdef', 1, 3);  // bcd
echo substr('abcdef', 0, 4);  // abcd
echo substr('abcdef', 0, 8);  // abcdef
echo substr('abcdef', -1, 1); // f
?>


/*
La funci�n "strstr" encuentra la primera aparici�n de un string.
Ejemplo:
*/
<?php
$email  = 'name@example.com';
$domain = strstr($email, '@');
echo $domain; // mostrar� @example.com
?>


/*
La funci�n "strpos" encuentra la posici�n de la primera ocurrencia de un substring en un string.
Ejemplo:
*/
<?php
$mystring = 'abc';
$findme   = 'a';
$pos = strpos($mystring, $findme);

if ($pos === false) {
    echo "La cadena '$findme' no fue encontrada en la cadena '$mystring'";
} else {
    echo "La cadena '$findme' fue encontrada en la cadena '$mystring'";
    echo " y existe en la posici�n $pos";
}
?>


/*
La funci�n "implode" une elementos de un array en un string.
Ejemplo:
*/
<?php

$array = array('apellido', 'email', 'tel�fono');
$separado_por_comas = implode(",", $array);

echo $separado_por_comas; // apellido,email,tel�fono

var_dump(implode('hola', array())); // string(0) ""

?>


/*
La funci�n "explode" divide un string en varios string.
Ejemplo:
*/
<?php
$str = "Hello world. It's a beautiful day.";
print_r (explode(" ",$str));
?>


/*
La funci�n "utf8_encode" codifica un string ISO-8859-1 a UTF-8.
Ejemplo:
*/
<?php
echo utf8_encode("Esta cadena est� codificada en UTF8");
?>


/*
La funci�n "utf8_decode" convierte una cadena con los caracteres codificados ISO-8859-1 con UTF-8 a un sencillo byte ISO-8859-1.
Ejemplo:
*/
<?php
$cadena = utf8_encode("Esta cadena est� codificada en UTF8");
echo utf8_decode($cadena);
?>


/*
La funci�n "array_pop" extrae el �ltimo elemento del final del array.
Ejemplo:
*/
<?php
$a=array("red","green","blue");
array_pop($a);
print_r($a);
?>


/*
La funci�n "array_push" inserta uno o m�s elementos al final de un array.
Ejemplo:
*/
<?php
$a=array("red","green");
array_push($a,"blue","yellow");
print_r($a);
?>


/*
La funci�n "array_diff" calcula la diferencia entre arrays.
Ejemplo:
*/
<?php
$a1=array("a"=>"red","b"=>"green","c"=>"blue","d"=>"yellow");
$a2=array("e"=>"red","f"=>"green","g"=>"blue");

$result=array_diff($a1,$a2);
print_r($result);
?>


/*La funci�n "array_walk" aplicar una funci�n proporcionada por el usuario a cada miembro de un array.
Ejemplo:
*/
<?php
function myfunction($value,$key)
{
echo "The key $key has the value $value<br>";
}
$a=array("a"=>"red","b"=>"green","c"=>"blue");
array_walk($a,"myfunction");
?>


/*
La funci�n "sort" ordena un array.
Ejemplo:
*/
<?php

$frutas = array("lim�n", "naranja", "banana", "albaricoque");
sort($frutas);
foreach ($frutas as $clave => $valor) {
    echo "frutas[" . $clave . "] = " . $valor . "\n";
}

?>


/*
La funci�n "current" devuelve el elemento actual en un array.
Ejemplo:
*/
<?php
$people = array("Peter", "Joe", "Glenn", "Cleveland");

echo current($people);
?>


/*
La funci�n "date" dar formato a la fecha/hora local.
Ejemplo:
*/
<?php
echo date('l \t\h\e jS');
?>


/*
La funci�n "empty" determina si una variable est� vac�a.
Ejemplo:
*/
<?php
$expected_array_got_string = 'somestring';
var_dump(empty($expected_array_got_string['some_key']));
var_dump(empty($expected_array_got_string[0]));
var_dump(empty($expected_array_got_string['0']));
var_dump(empty($expected_array_got_string[0.5]));
var_dump(empty($expected_array_got_string['0.5']));
var_dump(empty($expected_array_got_string['0 Mostel']));
?>


/*
La funci�n "isset" determina si una variable est� definida y no es NULL.
Ejemplo:
*/
<?php

$a = array ('test' => 1, 'hello' => NULL, 'pie' => array('a' => 'apple'));

var_dump(isset($a['test']));            // TRUE
var_dump(isset($a['foo']));             // FALSE
var_dump(isset($a['hello']));           // FALSE

// La clave 'helo' es igual a NULL as� que no se considera definida
// Si desea comprobar los valores NULL clave, intente:
var_dump(array_key_exists('hello', $a)); // TRUE

// Comprobando valores de arrays con m�s profunidad
var_dump(isset($a['pie']['a']));        // TRUE
var_dump(isset($a['pie']['b']));        // FALSE
var_dump(isset($a['cake']['a']['b']));  // FALSE

?>


/*
La funci�n "serialize" genera una representaci�n apta para el almacenamiento de un valor.
Ejemplo:
*/
<?php
$serialized_data = serialize(array('Math', 'Language', 'Science'));
echo  $serialized_data ;
?>


/*
La funci�n "unserialize" crea un valor PHP a partir de una representaci�n almacenada.
Ejemplo:
*/
<?php 

$a= array("1","2","3"); 
    
	print_r($a); 

$b=serialize($a); 
    
	echo $b; 

$c=unserialize($b); 
    
	print_r($c);

?>