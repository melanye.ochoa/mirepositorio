<?php

class Carro{
	//declaracion de propiedades
	public $color;
	
}


//crea aqui la clase Moto junto con dos propiedades public
class Moto{
	public $modelo;
	public $marca;
}

//inicializamos el mensaje que lanzara el servidor con vacio
$mensajeServidor='';


//crea aqui la instancia o el objeto de la clase Moto
$moto = new Moto;

$Carro1 = new Carro;

 if ( !empty($_POST)){

 	//almacenamos el valor mandado por POST en el atributo color
 	$Carro1->color=$_POST['color'];
 	$moto->modelo=$_POST['modelo'];
 	$moto->marca=$_POST['marca'];

 	//se construye el mensaje que sera lanzado por el servidor
 	$mensajeServidor='el servidor dice que ya escogiste un color: '.$_POST['color'];
 
 	 // recibe aqui los valores mandados por post 
 	$marca = $_POST['marca'];
 	$modelo = $_POST['modelo'];
 }  

?>

<!DOCTYPE html>
<html>
<head>

	<link rel="stylesheet" href="../css/bootstrap.min.css">
	<link rel="stylesheet" href="../css/bootstrap-grid.css">
	<script type="text/javascript" src="../js/bootstrap.min.js"></script>
	<script type="text/javascript" src="../js/jquery-3.4.1.min.js"></script>
	<title>
		Indice
	</title>
</head>
<body>
	
	<input type="text" class="form-control" value="<?php  echo $mensajeServidor; ?>" readonly>

	<!-- aqui puedes insertar el mesaje del servidor para Moto-->
	<label for="CajaTexto4">Resultado:</label>
	<div class="moto3">
	<input class="form-control" type="color" name="resultado" id="CajaTexto4" value="<?php 
	echo '<br/> La marca de la moto es:' . $marca . 'y el modelo es:' . $modelo;
	?>">
	</div>

	<div class="container" style="margin-top: 4em">
	
	<header> <h1>Carro y Moto</h1></header><br>
	<form method="post">
		<div class="form-group row">

			<label class="col-sm-3" for="CajaTexto1">Color del carro:</label>
			<div class="col-sm-4">
					<input class="form-control" type="color" name="color" id="CajaTexto1">
			</div>
			<div class="col-sm-4">
			</div>

			<!-- inserta aqui los inputs para recibir los atributos del objeto-->
			<label for="CajaTexto2">Marca de la moto:</label>
			<div class="moto1">
			<input class="form-control" type="color" name="marca" id="CajaTexto2">
			</div>

			<label for="CajaTexto3">Modelo de la moto:</label>
			<div class="moto2">
			<input class="form-control" type="color" name="modelo" id="CajaTexto3">
			</div>

			
						
		</div>
		<button class="btn btn-primary" type="submit" >Enviar</button>
		<a class="btn btn-link offset-md-8 offset-lg-9 offset-6" href="../index.php">Regresar</a>
	</form>

	</div>


</body>
</html>