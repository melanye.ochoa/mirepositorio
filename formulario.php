<html>
<head>
  <title>Formulario</title>
  <meta charset="UTF-8">
</head>

 <style>
    h1 { color: orange; }
    input[type='radio']:after {
        width: 15px;
        height: 15px;
        border-radius: 15px;
        top: -2px;
        left: -1px;
        position: relative;
        background-color: #d1d3d1;
        content: '';
        display: inline-block;
        visibility: visible;
        border: 2px solid white;
    }

    input[type='radio']:checked:after {
        width: 15px;
        height: 15px;
        border-radius: 15px;
        top: -2px;
        left: -1px;
        position: relative;
        background-color: #ffa500;
        content: '';
        display: inline-block;
        visibility: visible;
        border: 2px solid white;
    }

  </style>

<h1>Registro de alumno</h1>

<p style="color:red;"><strong> Número de cuenta: </strong>
<br><input type="text" name="numCta" size="9" maxlength="9"><br><p>
<p style="color:red;"><strong> Nombre: </strong>
<br><input type="text" name="nombre" size="40" maxlength="40"><br><p>
<p style="color:red;"><strong> Primer apellido: </strong>
<br><input type="text" name="a_paterno" size="40" maxlength="40"><br><p>
<p style="color:red;"><strong> Segundo apellido: </strong>
<br> <input type="text" name="a_materno" size="40" maxlength="40"><br><p>
<p style="color:red;"><strong> Género: </strong>
<br>
<input type="radio" name="gender" value="male"> M<br>
<input type="radio" name="gender" value="female"> F<br>
<input type="radio" name="gender" value="other"> Otro
<p>
<p style="color:red;"><strong> Fecha de nacimiento: </strong>
<br><input type="text" name="fecNac" placeholder="dd/mm/aaaa" size="8" maxlength="8"><p>

<input type="submit" value="Enviar"><p>
</form>

<br />

</body>
</html>
